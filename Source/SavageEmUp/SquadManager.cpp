
#include "SquadManager.h"


ASquadManager::ASquadManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASquadManager::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnSquad();
}

void ASquadManager::SpawnSquad()
{
	for (int i = 0; i < squadSize; ++i) {
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = GetInstigator();
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		spawnParams.Name = FName("SquadCharacter" + i);
		FVector pos = FVector(300, 300, 100) + FVector::ForwardVector * 300 * i;
		squadCharacters.Add(GetWorld()->SpawnActor<ASquadCharacter>(squadCharacterBP, pos, FRotator::ZeroRotator, spawnParams));
	}
}

void ASquadManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

