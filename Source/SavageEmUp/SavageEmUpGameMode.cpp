// Copyright Epic Games, Inc. All Rights Reserved.

#include "SavageEmUpGameMode.h"
#include "SavageEmUpPlayerController.h"
#include "SavageEmUpCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASavageEmUpGameMode::ASavageEmUpGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASavageEmUpPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}