// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CharacterAgent.h"
#include "KeyboardController.generated.h"

/**
 * 
 */
UCLASS()
class SAVAGEEMUP_API AKeyboardController : public APlayerController
{
	GENERATED_BODY()

    //ACharacterAgent* agent = nullptr;
	
public:
    AKeyboardController();
    virtual void SetupInputComponent() override;
    virtual void Tick(float DeltaTime) override;
};
