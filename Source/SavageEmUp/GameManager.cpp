

#include "GameManager.h"

AGameManager::AGameManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AGameManager::BeginPlay()
{
	Super::BeginPlay();

	SpawnSquadManager();
}

void AGameManager::SpawnSquadManager()
{
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	spawnParams.Instigator = GetInstigator();
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	squadManager = GetWorld()->SpawnActor<ASquadManager>(squadManagerBP, spawnParams);
}

void AGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

