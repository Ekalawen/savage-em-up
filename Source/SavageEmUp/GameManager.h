
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SquadManager.h"
#include "GameManager.generated.h"

UCLASS()
class SAVAGEEMUP_API AGameManager : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
    TSubclassOf<class ASquadManager> squadManagerBP;

	ASquadManager* squadManager;
	
public:	
	AGameManager();

	virtual void BeginPlay() override;

	void SpawnSquadManager();

	virtual void Tick(float DeltaTime) override;

};
