// Copyright Epic Games, Inc. All Rights Reserved.

#include "SavageEmUp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SavageEmUp, "SavageEmUp" );

DEFINE_LOG_CATEGORY(LogSavageEmUp)
 