
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterAgent.generated.h"

UCLASS()
class SAVAGEEMUP_API ACharacterAgent : public ACharacter
{
	GENERATED_BODY()

public:	

	UPROPERTY(EditAnywhere)
    float speed = 100.0f;
	
	ACharacterAgent();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

};
