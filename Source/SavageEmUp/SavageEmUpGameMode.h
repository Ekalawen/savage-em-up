// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SavageEmUpGameMode.generated.h"

UCLASS(minimalapi)
class ASavageEmUpGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASavageEmUpGameMode();
};



