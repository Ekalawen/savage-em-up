
#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SquadCharacter.h"
#include "SquadManager.generated.h"

using namespace std;

UCLASS()
class SAVAGEEMUP_API ASquadManager : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ASquadCharacter> squadCharacterBP;

	UPROPERTY(EditAnywhere)
	int squadSize = 4;

	TArray<ASquadCharacter*> squadCharacters;

public:	

	ASquadManager();

	virtual void BeginPlay() override;

	void SpawnSquad();

	virtual void Tick(float DeltaTime) override;

};
